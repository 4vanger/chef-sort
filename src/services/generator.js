import app from '../module';

const getRandomString = () => {
	const length = Math.abs(Math.random()*8) + 2;
	return Math.random().toString(36).substr(2, length);
};

const createRandomEmail = () => `${getRandomString()}@${getRandomString()}.com`;

app.value('generator', (number) => {
			if(typeof number === 'undefined' || number === 0) {
				return [];
			}
			let email = createRandomEmail();
			const res = [email];
			while(res.length < number) {
				// add about 50% of reclaimed emails
				if(Math.random() >= 0.5) {
					// add copies of previously added email address
					res.push(email);
				} else {
					email = createRandomEmail();
					res.push(email);
				}
			}
			// shake, do not stir
			// quick code to shuffle data a little bit - if we need real shuffle - we could get something better here
			return res.sort(function() {
				return 0.5 - Math.random();
			});
});