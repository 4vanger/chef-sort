import app from '../module';

app.value('filterObject', emails => {
	const obj = {};
	for(let ii = 0; ii < emails.length; ii++) {
		obj[emails[ii]] = null;
	}
	return Object.keys(obj);
});