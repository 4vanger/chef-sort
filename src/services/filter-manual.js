import app from '../module';

app.value('filterManual', emails => {
	const memory = {};
	const res = [];
	for(let ii = 0; ii < emails.length; ii++) {
		const email = emails[ii];
		if(!memory[email]) {
			res.push(email);
			memory[email] = true;
		}
	}

	return res;
});