import app from '../module';

app.value('filterSet', emails => {
	const set = new Set(emails);
	return Array.from(set);
});