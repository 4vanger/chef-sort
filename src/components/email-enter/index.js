import app from '../../module';

app.component('emailEnter', {
	bindings: {
		emails: '<',
		setEmails: '&',
		gotoUnification: '&',
	},
	template: require('./template.html'),
	controller: ($scope, generator) => {
		$scope.generate = (amount) => $scope.$ctrl.setEmails({emailsList: generator(amount)});
	},
});