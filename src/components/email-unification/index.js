import app from '../../module';

// Use Performance API is available for more accurate results
const now = () => performance ? performance.now() : (new Date()).getTime();

app.component('emailUnification', {
	template: require('./template.html'),
	bindings: {
		emails: '<',
		gotoEntry: '&',
	},
	controller: ($scope, filterSet, filterObject, filterManual) => {
		// which method to use for filtering
		$scope.method = null;
		$scope.retries = 100;

		$scope.showResults = false;
		$scope.filteredResults = null;
		$scope.avgExecutionTime = null;

		$scope.setShowResults = value => $scope.showResults = value;

		// Reset results when method changes
		$scope.$watch('method', () => {
			$scope.showResults = false;
			$scope.filteredResults = null;
			$scope.avgExecutionTime = null;
		});

		const filtersMap = {
			set: filterSet,
			manual: filterManual,
			object: filterObject,
		};

		$scope.filter = () => {
			if($scope.filterForm.$invalid) {
				// Do nothing unless form has valid data
				return;
			}
			$scope.showResults = false;

			let totalExecutionTime = 0;
			const filterService = filtersMap[$scope.method];
			for(let ii = 0; ii < $scope.retries; ii ++){
				let start = now();
				filterService($scope.$ctrl.emails);
				totalExecutionTime += now() - start;
			}
			$scope.avgExecutionTime = totalExecutionTime / $scope.retries;
			$scope.filteredResults = filterService($scope.$ctrl.emails);
		}
	},
});