import app from '../../module'

app.component('app', {
	template: require('./template.html'),
	controller: ($scope) => {
		$scope.pane = 'enter';
		$scope.emails = [];
		$scope.gotoPane = pane => $scope.pane = pane;
		$scope.updateEmails = emails => $scope.emails = emails;
	}
});