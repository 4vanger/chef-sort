import app from '../module';
import {validate} from 'email-validator';

app.directive('emailsList', () => {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
			ctrl.$validators.validateEmailsList = (modelValue) => {
				if(!modelValue) {
					return true;
				}
				// validate that every value is valid email address
				// optimization - if one of the values is invalid - others won't be checked
				return modelValue.reduce((memo, value) => memo && validate(value), true);
			};
			ctrl.$parsers.push((value) => {
				if(value) {
					return value.split('\n');
				}
			});

			ctrl.$formatters.push((value) => {
				if(value) {
					return value.join('\n');
				}
			});
		}
	};
});