# App design explained
* Views are implemented as components for correct data&logic isolation
* `ng-router` was not used to not to over-complicate solution
* sources built in dev mode - all resources are casted to JS for simplification
* I didn't have a task to optimize resulting file size (I know about tree-shaking, UglifyJS etc)
* Didn't have enough time to use correct plural forms everywhere :(

# Installation
Use `nvm` and run `nvm use` or use latest Node 6.
`npm install`

# Building
`npm start`
Sources will be generated to `dist/` folder.
