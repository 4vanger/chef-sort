const webpack = require("webpack");
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

let webpackConfig = {
	entry: './src/entry.js',
	output: {
		path: path.resolve(__dirname, 'dist/'),
		filename: 'main.js',
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Sorting problem',
			inject: true,
			hash: true,
			template: './index.ejs',
		}),
	],
	module: {
		rules: [
			{
				test: /\.html$/,
				use: [
					{
						loader: 'raw-loader',
					},
				],
			},
			{
				test: /\.js/,
				use: [
					{
						// ES6 -> JS
						loader: 'babel-loader',
            options: {
                presets: ['env'],
            }
					},
					{
						// annotate angular DI injections so files caFn be minimized safely
						loader: 'ng-annotate-loader',
					},
				],
			},
			{
				test: /\.sass$/,
				use: [
					{
						loader: "style-loader", // creates style nodes from JS strings
					},
					{
						loader: "css-loader",// translates CSS into CommonJS
						options: {
							sourceMap: true,
						},
					},
					{
						loader: "sass-loader", // compiles Sass to CSS
						options: {
							sourceMap: true,
						},
					},
				],
			},
			{
				test: /\.(png|woff|woff2|eot|ttf|svg)$/,
				use: {
					loader: 'url-loader?limit=100000',
				},
			},
		],
	},
	devtool: 'source-map',
	watchOptions: {
		ignored: '/node_modules/',
	},
};

module.exports = webpackConfig;